# frozen_string_literal: true

require 'socket'

# Bridges a websocket connection to a TCP socket.
class Bridge
  def initialize(web_socket, data)
    port = data['port'].to_i
    raise(Errno::ECONNREFUSED, 'Invalid port number') unless port.positive?

    @web_socket = web_socket
    @tcp_socket = TCPSocket.new(data['host'], port) # Errno::ECONNREFUSED
    @tcp_thread = Thread.new { tcp_receive }
  end

  def receive(data)
    @tcp_socket.send(data, 0)
  end

  def close
    @tcp_thread.kill
  end

  private

  def tcp_receive
    loop do
      data = @tcp_socket.recv(1024)
      next @web_socket.send(data) unless data == ''

      @web_socket.close
      @tcp_thread.exit
    end
  end
end
