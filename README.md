WebSocket-to-TCP proxy
======================

...because, for some reason, web browsers still don't support raw TCP sockets.

Starting a server
-----------------

Make sure you have the `em-websocket` gem installed:

    gem install em-websocket

..then run the server:

    ruby server.rb

You can change the hostname and port using environment variables:

    HOST=127.0.0.1 PORT=1997 ruby server.rb

Otherwise, the host will default to `0.0.0.0` and the port will be `1337`.

Connecting from a client
------------------------

Websocket connections are expected to specify a host and port when connecting:

    let webSocket = new WebSocket('ws://0.0.0.0:1337?host=djxmmx.net&port=17')

These will be used to create a bridged TCP socket for the websocket connection.

How is this different to [websockify](https://github.com/kanaka/websockify)?
----------------------------------------------------------------------------

Websockify forwards websocket connections to a _fixed_ target (i.e. one defined
by the server), whereas this project allows clients to specify any host/port as
their target. This allows it to act as a more "generic" websocket-to-TCP proxy,
which I don't think websockify supports. (Please let me know if I'm wrong!)

TODO
----

+ Add some RSpec tests; there are almost certainly a few bugs at the moment.
+ Allow the list of target domains/ports to be restricted via a config file.

License
-------

Copyright (c) 2020 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
