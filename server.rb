# frozen_string_literal: true

require 'cgi'
require 'em-websocket'
require_relative './bridge.rb'

HOST = ENV['HOST'] || '0.0.0.0'
PORT = ENV['PORT'] || 1337
puts "Starting a websocket server on #{HOST}:#{PORT}"

def params_from(handshake)
  params = CGI.parse(handshake.query_string)
  params.transform_values { |v| v.one? ? v[0] : v }
end

bridge = {}
EM.run do
  EM::WebSocket.run(host: HOST, port: PORT) do |s|
    s.onopen do |handshake|
      puts "New connection: #{handshake.query_string}"
      bridge[s.object_id] = Bridge.new(s, params_from(handshake))
    rescue Errno::ECONNREFUSED => e
      puts(e.to_s)
      s.close(1011, e.to_s) # See https://tools.ietf.org/html/rfc6455
    end

    s.onmessage { |data| bridge[s.object_id]&.receive(data) }
    s.onclose { bridge.delete(s.object_id)&.close }
  end
end
